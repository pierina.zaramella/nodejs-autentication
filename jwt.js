const expressJwt = require('express-jwt');

const jwt = () => {
    const secret  = process.env.secret;
    return expressJwt({ secret, expiresIn:2 }).unless({
        path: [
            // public routes that don't require authentication
            '/users/register',
            '/users/login'
        ]
    });
}

module.exports = jwt;